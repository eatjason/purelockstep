﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityBehavior : MonoBehaviour
{
    public int index;
    public short tx;
    public short ty;

    // Start is called before the first frame update
    void Start()
    {
   
    }

    // Update is called once per frame
    void Update()
    {
        var game = transform.parent.gameObject.GetComponent<Controller>().game;

        var b = game.Bodies[index];
        var a = game.Animators[index];

        float angle = (float)b.angle + Mathf.PI / 2.0f;

        //Debug.Log($"{a.node}, {game.data.FrameTable[a.node].frame}");

        transform.position = new Vector3((float)b.position.x, (float)b.position.y, 0.0f);
        transform.rotation = Quaternion.AngleAxis(angle * 180.0f / Mathf.PI, Vector3.forward);
        // local scale
        // color
        GetComponent<SpriteRenderer>().sprite = transform.parent.GetComponent<Controller>().sprite_list[(int)(game.data.FrameTable[a.node].image)];
        //GetComponent<SpriteRenderer>().sprite = transform.parent.GetComponent<Controller>().sprite_list[0];

        var c = game.Components[index];
        if (game.TestMask(index, Cp.Player))
        {
            if (game.Players[index].slot == transform.parent.gameObject.GetComponent<Controller>().LocalSlot)
            {
                tx = game.Slots[game.Players[index].slot].Input.X;
                ty = game.Slots[game.Players[index].slot].Input.Y;
                //transform.rotation = Quaternion.AngleAxis((float)b.angle * 180.0f / Mathf.PI, Vector3.forward);
                //GetComponent<SpriteRenderer>().sprite = transform.parent.GetComponent<Controller>().sprite_list[(int)Data.Img.Circle_1];
            }
        }
        
    }

}
