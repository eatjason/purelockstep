﻿
using FixedMath;


public static class Const
{
	// integers
	static public Scaler vn1 = -1;
	static public Scaler v0 = 0;
	static public Scaler v1 = 1;
	static public Scaler v2 = 2;
	static public Scaler v4 = 4;
	static public Scaler v6 = 6;

	// decimal
	static public Scaler v0_22 = (Scaler)0.22f;
	static public Scaler v0_1 = (Scaler)0.1f;
	static public Scaler v0_04 = (Scaler)0.04f;
	static public Scaler v0_02333333333333333 = (Scaler)0.02333333333333333f;
	static public Scaler v0_00005 = (Scaler)0.00005f;
	static public Scaler v0_02 = (Scaler)0.02f;
	static public Scaler v0_2 = (Scaler)0.2f;
	static public Scaler v0_15 = (Scaler)0.15f;
	static public Scaler v0_003 = (Scaler)0.003f;
	static public Scaler v0_3 = (Scaler)0.3f;
	static public Scaler v0_062 = (Scaler)0.062f;
	static public Scaler v0_25 = (Scaler)0.25f;

	// fractions
	static public Scaler v1over100 = (Scaler)(1.0f / 100.0f);
}




public class Data
{

	public enum Img : ushort
	{
		Null_0 = 0,
		Bag_0,
		Blood_0,
		Blood_1,
		Blood_2,
		Blood_3,
		Blood_4,
		Blood_5,
		Blood_6,
		Blood_7,
		Boom_0,
		Boom_1,
		Boom_2,
		Boom_3,
		Boom_4,
		Bullet_0,
		Fat_0,
		Fat_1,
		Fat_2,
		Fat_3,
		Gas_0,
		Gas_1,
		Gas_2,
		Gas_3,
		Green_0,
		Green_1,
		Green_2,
		Green_3,
		Green_4,
		Green_5,
		Green_6,
		Green_7,
		Grenade_0,
		Man_0,
		Man_1,
		Man_2,
		Man_3,
		Rubber_0,
		Shirt_0,
		Shirt_1,
		Shirt_2,
		Shirt_3,
		Shot_0,
		Target_0,
		Yellow_0,
		Yellow_1,
		Yellow_2,
		Yellow_3,
		Yellow_4,
		Yellow_5,
		Yellow_6,
		Yellow_7,
		Circle_0,
		Circle_1
	}

	public enum Ani : ushort
	{
		Null = 0,
		ManIdle,
		ManShoot,
		GreenRun,
		Bullet,
		BoomPow,
		BoomWham,
		BoomKapow,
		BoomCrack,
		BoomDeath,
		Count,
		Self,
		Next,
		None,
	}

	public enum Action : ushort
	{
		None = 0,
		Destroy,
		Count,
	}

	public struct FrameNode
	{
		public Ani frame;
		public Img image;
		public Ani next;
		public ushort durration;
		public Action action;
	}

	public ushort[] AnimationTable = new ushort[(int)Ani.Count];

	public FrameNode[] FrameTable = new FrameNode[] {
		//			animation value,				image,						next,					dur,					action
		new FrameNode{frame = Ani.Null,             image = Img.Null_0,         next = Ani.Self,        durration = 0,          action = Action.Destroy},

		new FrameNode{frame = Ani.ManIdle,			image = Img.Man_0,           next = Ani.Self,        durration = 0,          action = Action.None},

		new FrameNode{frame = Ani.ManShoot,          image = Img.Man_0,           next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Man_1,           next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Man_2,           next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Man_3,           next = Ani.ManIdle,	  durration = 2,          action = Action.None},

		new FrameNode{frame = Ani.GreenRun,          image = Img.Green_0,         next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Green_1,         next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Green_2,         next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Green_3,         next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Green_4,         next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Green_5,         next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Green_6,         next = Ani.Next,        durration = 2,          action = Action.None},
		new FrameNode{frame = Ani.None,              image = Img.Green_7,         next = Ani.GreenRun,    durration = 2,          action = Action.None},

		new FrameNode{frame = Ani.BoomPow,           image = Img.Boom_0,          next = Ani.Null,        durration = 10,         action = Action.None},
		new FrameNode{frame = Ani.BoomWham,          image = Img.Boom_1,          next = Ani.Null,        durration = 10,         action = Action.None},
		new FrameNode{frame = Ani.BoomKapow,         image = Img.Boom_2,          next = Ani.Null,        durration = 10,         action = Action.None},
		new FrameNode{frame = Ani.BoomCrack,         image = Img.Boom_3,          next = Ani.Null,        durration = 10,         action = Action.None},
		new FrameNode{frame = Ani.BoomDeath,         image = Img.Boom_4,          next = Ani.Null,        durration = 10,         action = Action.None},

		new FrameNode{frame = Ani.Bullet,            image = Img.Bullet_0,        next = Ani.Null,        durration = 100,        action = Action.None},

	};
	
	public Data()
	{
		for (ushort i = 0; i < FrameTable.Length; ++i)
		{
			var name = FrameTable[i].frame;
			if (name != Ani.None)
			{
				AnimationTable[(int)name] = i;
			}
		}

		for (ushort i = 0; i < FrameTable.Length; ++i)
		{
			switch (FrameTable[i].next)
			{
				case Ani.Self:	FrameTable[i].next = (Ani)i;		break;
				case Ani.Next:	FrameTable[i].next = (Ani)i + 1;	break;
				default: FrameTable[i].next = (Ani)AnimationTable[(int)FrameTable[i].next]; break;
			}
		}
	}


	public ushort GetNode(Ani a)
	{
		return AnimationTable[(int)a];
	}

}


/*
func makeKey(a uint8, b uint8) uint16 {
	var r uint16
	if a > b {
		r = (uint16(a) << 8) | uint16(b)
	} else {
		r = (uint16(b) << 8) | uint16(a)
	}
	return r
}
*/