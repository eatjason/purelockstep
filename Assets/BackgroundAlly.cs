﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAlly : MonoBehaviour
{
    public GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        int count = 5;
        GameObject go;
        for (int j = -6 * count; j <= 6 * count; j += 6)
        {
            for (int i = -8 * count; i <= 8 * count; i += 8)
            {
                go = UnityEngine.Object.Instantiate(prefab);
                go.transform.parent = gameObject.transform;
                go.transform.localPosition = new Vector3(i, j, 0.0f);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
