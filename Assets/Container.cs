﻿//using System.Collections;
//using System.Collections.Generic;



public struct Reference<T>
{
    private T[] list;
    private int index;

    public Reference(T[] l, int i)
    {
        list = l;
        index = i;
    }

    public bool IsNull
    {
        get { return list == null; }
    }

    public T Copy
    {
        get { return list[index]; }
    }

    public ref T Ref
    {
        get { return ref list[index]; }
    }
}


public class ValueSet
{
    static public int EndOfList = -1;

    private int head;
    private int size;
    private int[] list;

    public void Resize(int s)
    {
        size = 0;
        head = EndOfList;
        list = new int[s];
    }

    public int Remove()
    {
        int value = head;
        if (value == EndOfList)
        {
            if (size < list.Length)
            {
                value = size;
                ++size;
                list[value] = EndOfList;
            }
        }
        else
        {
            head = list[value];
            list[value] = EndOfList;
        }
        return value;
    }

    public void Restore(int value)
    {
        if (list[value] == EndOfList)
        {
            list[value] = head;
            head = value;
        }
    }
}


public class Vector<T> where T : struct
{
    private int size;
    private T[] list;

    public void Resize(int s)
    {
        size = 0;
        list = new T[s];
    }

    public void Clear()
    {
        size = 0;
    }

    public int Append()
    {
        int r = size;
        ++size;
        return r;
    }

    public Reference<T> Insert(int key)
    {
        if (size < key + 1)
            size = key + 1;
        return new Reference<T>(list, key);
    }

    public void Extend(int key)
    {
        if (size < key + 1)
            size = key + 1;
        // throw error if size > list length
    }

    public void Delete(int key)
    {
        --size;
        list[key] = list[size];
    }

    public Reference<T> Find(int key)
    {
        return new Reference<T>(list, key);
    }

    public ref T this[int i]
    {
        get { return ref list[i]; }
    }

    public int Size
    {
        get { return size; }
    }

    public int Capacity
    {
        get { return list.Length; }
    }

    public delegate bool LessThan(ref T lhs, ref T rhs);
    public void Sort(LessThan f)
    {
        // do things
        T swap;
        bool done = false;
        while (!done)
        {
            done = true;
            for (int i = 0; i < size - 1; ++i)
            {
                if (f(ref list[i + 1], ref list[i]) == true)
                {
                    swap = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = swap;
                    done = false;
                }
            }
        }
    }
 
}


/*8
public class HashMap
{
    static public int Empty = -1;
}

public class HashMap<T> : HashMap where T : struct
{
    //static public int Empty = -1;

    private struct FindData
    {
        public int index;
        public int empty;
    }

    private int count;
    private int[] keys;
    private T[] list;

    public void Resize(int s)
    {
        count = 0;
        keys = new int[s];
        list = new T[s];

        for (int i = 0; i < keys.Length; ++i)
        {
            keys[i] = Empty;
        }
    }

    public int Capacity
    {
        get { return keys.Length; }
    }

    private FindData FindIndex(int key)
    {
        FindData find = new FindData();
        find.empty = -1;
        find.index = -1;
        for (int i = 0; i < keys.Length; ++i)
        {
            if (keys[i] == Empty)
            {
                find.empty = i;
            }
            if (keys[i] == key)
            {
                find.index = i;
            }
        }
        return find;
    }

    public Reference<T> Insert(int key)
    {
        FindData find = FindIndex(key);
        if (find.index == -1)
        {
            find.index = find.empty;
        }
        if (find.index == -1)
        {
            return new Reference<T>(null, 0);
        }
        else
        {
            keys[find.index] = key;
            return new Reference<T>(list, find.index);
        }
    }

    public ref T this[int i]
    {
        get
        {
            FindData find = FindIndex(i);
            return ref list[find.index];
        }
    }

    public void Delete(int key)
    {
        FindData find = FindIndex(key);
        if (find.index != -1)
        {
            keys[find.index] = Empty;
        }
    }

    public bool Has(int key)
    {
        return Find(key).IsNull;
    }

    public Reference<T> Find(int key)
    {
        FindData find = FindIndex(key);
        if (find.index != -1)
        {
            return new Reference<T>(list, find.index);
        }
        else
        {
            return new Reference<T>(null, 0);
        }
    }

     //   These could use enumerables
      
    public int Key(int index)
    {
        return keys[index];
    }

    public ref T Get(int index)
    {
        return ref list[index];
    }
}*/
