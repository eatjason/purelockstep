﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FixedMath;

public class LocalEntity : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
   
    }

    // Update is called once per frame
    void Update()
    {
        var pos = transform.parent.gameObject.GetComponent<Controller>().dummy_pos;
        var angle = transform.parent.gameObject.GetComponent<Controller>().dummy_angle;
        transform.position = new Vector3((float)pos.x, (float)pos.y, -1.0f);
        transform.rotation = Quaternion.AngleAxis((float)angle * 180.0f / Mathf.PI, Vector3.forward);
    }

}
