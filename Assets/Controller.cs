﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using NativeWebSocket;
using FixedMath;
using UnityEngine;
using UnityEngine.UI;

public static class Util
{
    static public string ToBinaryStringUByte(byte value)
    {
        string ret = "";
        byte mask = 0x01;
        for (int i = 0; i < 8; ++i)
        {
            if ((value & mask) == mask)
            {
                ret += "1";
            }
            else
            {
                ret += "0";
            }
            mask = (byte)(mask << 1);
        }
        return ret;
    }

    static public string ToBinaryStringUShort(ushort value)
    {
        string ret = "";
        ushort mask = 0x01;
        for (int i = 0; i < 16; ++i)
        {
            if ((value & mask) == mask)
            {
                ret += "1";
            }
            else
            {
                ret += "0";
            }
            mask = (ushort)(mask << 1);
        }
        return ret;
    }
    
    static public sbyte SingleSignedByte(byte[] bytes)
    {
        sbyte r = -1;
        using (MemoryStream m = new MemoryStream(bytes))
        {
            using (BinaryReader reader = new BinaryReader(m))
            {
                r = reader.ReadSByte();
            } 
        }
        return r;
    }

    static public byte[] ToBinary(ref Control input)
    {
        using (MemoryStream m = new MemoryStream())
        {
            using (BinaryWriter writer = new BinaryWriter(m))
            {
                writer.Write(input.X);
                writer.Write(input.Y);
                ushort b = 0x0000;

                if (input.Left) b |= (0x1 << 0);
                if (input.Right) b |= (0x1 << 1);
                if (input.Up) b |= (0x1 << 2);
                if (input.Down) b |= (0x1 << 3);

                if (input.Primary) b |= (0x1 << 4);
                if (input.Secondary) b |= (0x1 << 5);

                if (input.Ready) b |= (0x1 << 6);
                if (input.Synced) b |= (0x1 << 7);
                if (input.Message) b |= (0x1 << 8);

                writer.Write(b);
            }
            return m.ToArray();
        }
    }

    static public void ToClass(Vector<Slot> slots, byte[] bytes)
    {
        using (MemoryStream m = new MemoryStream(bytes))
        {
            using (BinaryReader reader = new BinaryReader(m))
            {
                var active = reader.ReadUInt16();           
                for (int i = 0; i < 4; ++i)
                {
                    ref var slot = ref slots[i];
                    var mx = reader.ReadInt16();
                    var my = reader.ReadInt16();
                    var b = reader.ReadUInt16();
                    
                    slot.Connected = (active & (0x1 << i)) != 0x0;

                    slot.Input.X = mx;
                    slot.Input.Y = my;

                    slot.Input.Left = (b & (0x1 << 0)) != 0x0;
                    slot.Input.Right = (b & (0x1 << 1)) != 0x0;
                    slot.Input.Up = (b & (0x1 << 2)) != 0x0;
                    slot.Input.Down = (b & (0x1 << 3)) != 0x0;

                    slot.Input.Primary = (b & (0x1 << 4)) != 0x0;
                    slot.Input.Secondary = (b & (0x1 << 5)) != 0x0;
                    
                    slot.Input.Ready = (b & (0x1 << 6)) != 0x0;
                    slot.Input.Synced = (b & (0x1 << 7)) != 0x0;
                    slot.Input.Message = (b & (0x1 << 8)) != 0x0;
                }
            }
        }

    }

    public struct QueueEntry
    {
        public QueueEntry(byte[] b)
        {
            bytes = b;
        }
        public Byte[] bytes;
    }

}





public class Controller : MonoBehaviour
{
    static public string[] ImageNameTable = new string[] {
        "null_0",
        "bag_0",
        "blood_0",
        "blood_1",
        "blood_2",
        "blood_3",
        "blood_4",
        "blood_5",
        "blood_6",
        "blood_7",
        "boom_0",
        "boom_1",
        "boom_2",
        "boom_3",
        "boom_4",
        "bullet_0",
        "fat_0",
        "fat_1",
        "fat_2",
        "fat_3",
        "gas_0",
        "gas_1",
        "gas_2",
        "gas_3",
        "green_0",
        "green_1",
        "green_2",
        "green_3",
        "green_4",
        "green_5",
        "green_6",
        "green_7",
        "grenade_0",
        "man_0",
        "man_1",
        "man_2",
        "man_3",
        "rubber_0",
        "shirt_0",
        "shirt_1",
        "shirt_2",
        "shirt_3",
        "shot_0",
        "target_0",
        "yellow_0",
        "yellow_1",
        "yellow_2",
        "yellow_3",
        "yellow_4",
        "yellow_5",
        "yellow_6",
        "yellow_7",
        "circle_0",
        "circle_1"
    };

    public GameObject dummy;
    public FixedMath.Vector2 dummy_pos;
    public FixedMath.Vector2 dummy_displacement;
    public FixedMath.Vector2 dummy_velocity;
    public Scaler dummy_angle;

    public GameObject target;

    public bool ShowOutput;
    public GameObject output;

    public GameObject prefab;
    public GameObject mainCamera;
    public GameObject player;

    private Dictionary<string, ushort> ImageNameMap;
    public Dictionary<int, GameObject> table;
    public Sprite[] sprite_list;

    public WebSocket websocket;
    public sbyte LocalSlot;
    public Control GameInput;
    public Game game;
    public bool running;

    public Queue<Util.QueueEntry> InputQueue;

    // display should be 800x600!

    public float waitTime = 0.0f;
    public float deltaTime = 0.0f;
    public string deltaTimeText;
    void Awake()
    {
        //QualitySettings.vSyncCount = 0;  // VSync must be disabled
        //Application.targetFrameRate = 30;
    }

    // Start is called before the first frame update
    void Start()
    {
        ShowOutput = false;

        InputQueue = new Queue<Util.QueueEntry>();

        ImageNameMap = new Dictionary<string, ushort>();
        sprite_list = Resources.LoadAll<Sprite>("Images/output");
        for (int i = 0; i < ImageNameTable.Length; i++)
        {
            ImageNameMap[ImageNameTable[i]] = (ushort)i;
            for (int j = 0; j < sprite_list.Length; j++)
            {
                if (ImageNameTable[i] == sprite_list[j].name)
                {
                    Sprite swap = sprite_list[i];
                    sprite_list[i] = sprite_list[j];
                    sprite_list[j] = swap;
                }
            }
        }

        table = new Dictionary<int, GameObject>();

        running = false;
        player = null;

        game = new Game();
    }

    // Update is called once per frame
    async void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) == true) { JustLeave(); }
        if (Input.GetKeyDown(KeyCode.Alpha0) == true) { JoinOrLeave("0"); }
        if (Input.GetKeyDown(KeyCode.Alpha1) == true) { JoinOrLeave("1"); }
        if (Input.GetKeyDown(KeyCode.Alpha2) == true) { JoinOrLeave("2"); }
        if (Input.GetKeyDown(KeyCode.Alpha3) == true) { JoinOrLeave("3"); }
        if (Input.GetKeyDown(KeyCode.Alpha4) == true) { JoinOrLeave("4"); }
        if (Input.GetKeyDown(KeyCode.Alpha5) == true) { JoinOrLeave("5"); }
        if (Input.GetKeyDown(KeyCode.Alpha6) == true) { JoinOrLeave("6"); }
        if (Input.GetKeyDown(KeyCode.Alpha7) == true) { JoinOrLeave("7"); }
        if (Input.GetKeyDown(KeyCode.Alpha8) == true) { JoinOrLeave("8"); }
        if (Input.GetKeyDown(KeyCode.Alpha9) == true) { JoinOrLeave("9"); }

        if (Input.GetKeyDown(KeyCode.P) == true) { GameInput.Ready = true; }

        GameInput.Primary = Input.GetMouseButton(0);

        GameInput.Up = Input.GetKey(KeyCode.W);
        GameInput.Down = Input.GetKey(KeyCode.S);
        GameInput.Left = Input.GetKey(KeyCode.A);
        GameInput.Right = Input.GetKey(KeyCode.D);

        GameInput.X = (short)(Input.mousePosition.x - (Screen.width / 2));
        GameInput.Y = (short)(Input.mousePosition.y - (Screen.height / 2));


        if (Input.GetKeyDown(KeyCode.Tab) == true)
        {
            ShowOutput = !ShowOutput;
            //Debug.Log($"{ShowOutput} - {output.transform.parent.gameObject.name}");
            output.transform.parent.gameObject.SetActive(ShowOutput);
        }

        waitTime += Time.deltaTime;
        if (waitTime > 0.033f)
        {
            deltaTime += (waitTime - deltaTime) * 0.1f;
            deltaTimeText = Mathf.Ceil(1.0f / deltaTime).ToString();
            waitTime -= 0.033f;
        }
        else
        {
            return;
        }

        //deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        //deltaTimeText = Mathf.Ceil(1.0f / deltaTime).ToString();

        while (InputQueue.Count > 0)
        //if (InputQueue.Count > 0)
        {
            Util.ToClass(game.Slots, InputQueue.Dequeue().bytes);
            // update game state here

            if (running == true)
            {
                // START DUMMY
                dummy_pos += dummy_displacement;

                FixedMath.Vector2 move = new FixedMath.Vector2(Scaler.Zero, Scaler.Zero);

                if (Input.GetKey(KeyCode.A)) move.x = Const.vn1;
                if (Input.GetKey(KeyCode.D)) move.x = Const.v1;
                if (Input.GetKey(KeyCode.W)) move.y = Const.v1;
                if (Input.GetKey(KeyCode.S)) move.y = Const.vn1;

                move.Normalize();

                dummy_velocity = Const.v0_02333333333333333 * Const.v2 * move;

                var gx = (short)(Input.mousePosition.x - (Screen.width / 2));
                var gy = (short)(Input.mousePosition.y - (Screen.height / 2));

                var target = new FixedMath.Vector2(gx, gy);
                target.Normalize();

                dummy_angle = target.Angle();

                //Debug.Log($"{a.node}, {game.data.FrameTable[a.node].frame}");

                // local scale
                // color
                //GetComponent<SpriteRenderer>().sprite = transform.parent.GetComponent<Controller>().sprite_list[(int)Data.Img.Circle_1];

                dummy_displacement = dummy_velocity;
                var damping = Const.v0_003;
                var mag = dummy_velocity.Length();
                if (mag <= damping)
                {
                    dummy_velocity = new FixedMath.Vector2(Const.v0, Const.v0);
                }
                else
                {
                    dummy_velocity -= dummy_velocity.NormalizedCopy() * damping;
                }
                // END DUMMY


                game.Update();
            }
            else
            {
                int present = 0, ready = 0;
                for (int i = 0; i < 4; ++i)
                {
                    if (game.Slots[i].Connected == true) { ++present; }
                    if (game.Slots[i].Input.Ready == true) { ++ready; }
                }

                if (present == ready)
                {
                    game.OnCreate += (Entity entity) => {
                        if (entity != Entity.Null)
                        {
                            GameObject go = UnityEngine.Object.Instantiate(prefab);
                            go.GetComponent<EntityBehavior>().index = entity.index;
                            go.transform.parent = transform;
                            table.Add(entity.index, go);
                        }
                    };

                    game.OnDestroy += (Entity entity) => {
                        if (entity != Entity.Null)
                        {
                            GameObject go = table[entity.index];
                            table.Remove(entity.index);
                            UnityEngine.Object.Destroy(go);
                        }
                    };

                    game.Init(512);

                    for (int i = 0; i < game.Components.Size; ++i)
                    {
                        int index = i;
                        var c = game.Components[index];
                        if (game.TestMask(index, Cp.Player))
                        {
                            if (game.Players[index].slot == LocalSlot)
                            {
                                player = table[index];
                            }
                        }
                    }
               
                    dummy_pos = game.Bodies[player.GetComponent<EntityBehavior>().index].position;
                    dummy_displacement = game.Bodies[player.GetComponent<EntityBehavior>().index].displacement;
                    dummy_velocity = game.Bodies[player.GetComponent<EntityBehavior>().index].velocity;
                    dummy_angle = game.Bodies[player.GetComponent<EntityBehavior>().index].angle;
                    
                    running = true;
                }

            }
        }

        if (websocket != null && websocket.State == WebSocketState.Open)
        {
            await websocket.Send(Util.ToBinary(ref GameInput));
        }

        //Debug.Log($"{Input.mousePosition.x},{Input.mousePosition.y}");
    }

    void LateUpdate()
    {
        GameObject targ = player;
        //GameObject targ = dummy;

        if (player != null)
        {
            var tx = player.GetComponent<EntityBehavior>().tx;
            var ty = player.GetComponent<EntityBehavior>().ty;
            target.transform.localPosition = new Vector3(player.transform.position.x + (tx / 100.0f) * 2.0f, player.transform.position.y + (ty / 100.0f) * 2.0f, 0.0f);
            //target.transform.localPosition = new Vector3(player.transform.position.x + (GameInput.X / 100.0f) * 2.0f, player.transform.position.y + (GameInput.Y / 100.0f) * 2.0f, -5.0f);
        }

        if (targ != null)
        { 
            mainCamera.transform.localPosition = new Vector3(targ.transform.position.x + (GameInput.X / 100.0f), targ.transform.position.y + (GameInput.Y / 100.0f), -10.0f);
            //mainCamera.transform.position = new Vector3(player.transform.position.x + (GameInput.X / 100.0f), player.transform.position.y + (GameInput.Y / 100.0f), -10.0f);
            //mainCamera.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10.0f);
        }

        string s = "";
        for (int i = 0; i < 4; ++i)
        {
            ref var slot = ref game.Slots[i];
            s += $"{i}: {slot.Connected} - {slot.Input.X}, {slot.Input.Y} - {slot.Input.Ready}, {slot.Input.Synced},{slot.Input.Message},{slot.Input.Left},{slot.Input.Right},{slot.Input.Up},{slot.Input.Down},{slot.Input.Primary},{slot.Input.Secondary}\n\r";
        }
        s += $"mouse - {GameInput.X / 100.0f}, {GameInput.Y / 100.0f}\n\r";
        s += $"target - {mainCamera.transform.localPosition.x}, {mainCamera.transform.localPosition.y}\n\r";
        s += $"camera - {mainCamera.transform.localPosition.x}, {mainCamera.transform.localPosition.y}\n\r";
        s += $"camera2 - {mainCamera.transform.position.x}, {mainCamera.transform.position.y}\n\r";
        output.GetComponent<Text>().text = s;
    }

    private void JustLeave()
    {
        if (websocket != null)
        {
            Leave();
        }
    }

    private void JoinOrLeave(string s)
    {
        if (websocket == null)
        {
            Join(s);
        }
        else
        {
            Leave();
        }
    }

    async void Leave()
    {
        await websocket.Close();
        //websocket = null;
    }

    async void Join(string s)
    {
        websocket = new WebSocket($"wss://lock-step-pure.herokuapp.com/session/{s}");

        websocket.OnOpen += () => { Debug.Log("Connection open!"); };
        websocket.OnError += (e) => { Debug.Log("Error! " + e); };
        websocket.OnClose += (e) => { Debug.Log("Connection closed!"); };

        websocket.OnMessage += (bytes) => {
            // do something based on size of message!  set player slot is 1 byte.
            if (bytes.Length == 1)
            {
                LocalSlot = Util.SingleSignedByte(bytes);
                Debug.Log($"Assigned {LocalSlot}");
            }
            else
            {
                InputQueue.Enqueue(new Util.QueueEntry(bytes));
            }
        };

        // waiting for messages
        await websocket.Connect();
    }
}
