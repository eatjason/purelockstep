﻿using System.Collections;
using System.Collections.Generic;

using FixedMath;



public struct Control
{
    // 9 bits, 7 left over
    public bool Ready;
    public bool Synced;
    public bool Message;

    public bool Left;
    public bool Right;
    public bool Up;
    public bool Down;

    public bool Primary;
    public bool Secondary;

    // 4 bytes, 2 per dimension
    public short X;
    public short Y;
}

public struct Slot
{
    public bool Connected;
    public Control Input;
}

public struct Entity
{
    static public Entity Null = new Entity(0, -1);

    public Entity(uint g, int i)
    {
        generation = g;
        index = i;
    }

    public static bool operator ==(Entity lhs, Entity rhs)
    {
        return (lhs.generation == rhs.generation && lhs.index == rhs.index);
    }

    public static bool operator !=(Entity lhs, Entity rhs)
    {
        return (lhs.generation != rhs.generation || lhs.index != rhs.index);
    }

    public uint generation;
    public int index;
}


// component structs
public struct Animator
{
    public ushort count;
    public ushort node;
}

public struct Body
{
    public Vector2 position;
    public Vector2 velocity;
    public Vector2 displacement;
    public Scaler angle;

    public Scaler radius;
    public Scaler invMass;
    public Scaler damping;
}

public struct Player
{
    public sbyte slot;
    public ushort delayFire;
}

public struct Enemy
{
    public Entity target;
}

// buffer structs

public struct Contact
{
    static public Scaler SMALL_SLOP = Const.v0_00005;
    static public Scaler SLOP = Const.v0_02;

    public Scaler impulse;
    public Vector2 normal;
    public Scaler overlap;

    public Entity A;
    public Entity B;

    public Scaler invMassA;
    public Scaler invMassB;

    public Contact Invert()
    {
        Contact r = this;

        Entity swape = r.A;
        r.A = r.B;
        r.B = swape;

        Scaler swapi = invMassA;
        invMassA = invMassB;
        invMassB = swapi;

        r.normal = -r.normal;
        return r;
    }

    public bool Touch()
    {
        return (overlap - SLOP) <= Const.v0;
    }
}

public struct Bounds
{
    public sbyte screen;
    public Entity entity;
    public Vector2 lower;
    public Vector2 upper;

    public Bounds(Entity e, Vector2 p, Vector2 d, Scaler r)
    {
        screen = -1;
        entity = e;
        lower = new Vector2(
            p.x + Scaler.Min(Const.v0, d.x) - r - Contact.SLOP,
            p.y + Scaler.Min(Const.v0, d.y) - r - Contact.SLOP
        );
        upper = new Vector2(
            p.x + Scaler.Min(Const.v0, d.x) + r + Contact.SLOP,
            p.y + Scaler.Min(Const.v0, d.y) + r + Contact.SLOP
        );
    }

    public bool Overlap(ref Bounds b)
    {
        if (b.lower.x > upper.x || b.upper.x < lower.x ||
            b.lower.y > upper.y || b.upper.y < lower.y)
        {
            return false;
        }
        return true;
    }
}



public enum ObjType : byte
{
    Player = 0,
    Enemy = 1,
    Bullet = 2,
}

public enum Cp : ushort
{
    Active = 1 << 0,
    Component = 1 << 1,
    Generation = 1 << 2,
    Body = 1 << 3,
    Type = 1 << 4,
    Animator = 1 << 5,
    Player = 1 << 6,
    Enemy = 1 << 7,
}


public class Game
{
    // delegates for websocket events
    public delegate void EntityDel(Entity e);

    // delegates for actions and collisions
    public delegate void ActionDel(Game g, int i, ref Animator a);
    public delegate void CollisionDel(Game g, ref Contact c);

    // events for websocket
    public event EntityDel OnCreate;
    public event EntityDel OnDestroy;

    // Data and tables
    public Random Rand;
    public Data data;
    public ActionDel[] ActionTable;
    public Dictionary<ushort, CollisionDel> CollisionTable;

    // slots for players
    public Vector<Slot> Slots;

    // global vars
    //public Entity player;
    
    public int BadieCount;

    // max entities
    public int MaxActivate;
    public int MaxContact;
    public int MaxEntity;

    // entity id manager
    public ValueSet EntityManager;

    // list of entities that were created this frame
    public Vector<int> Activated;

    // arrays of componenets
    public Vector<ushort> Components;
    public Vector<uint> Generation;
    public Vector<byte> ObjectType;
    public Vector<Animator> Animators;
    public Vector<Body> Bodies;
    public Vector<Player> Players;
    public Vector<Enemy> Enemies;

    // buffers
    public Vector<Bounds> BoundList;
    public Vector<Contact> Contacts;
    public Vector<Vector2> Targets;

    public Game()
    {
        Rand = new Random();
        Rand.SetSeed(0x03ff31a4);

        data = new Data();
        ActionTable = new ActionDel[(int)Data.Action.Count];
        CollisionTable = new Dictionary<ushort, CollisionDel>();

        Slots = new Vector<Slot>();
        EntityManager = new ValueSet();

        Activated = new Vector<int>();

        Components = new Vector<ushort>();
        Generation = new Vector<uint>();
        ObjectType = new Vector<byte>();
        Bodies = new Vector<Body>();
        Players = new Vector<Player>();
        Enemies = new Vector<Enemy>();
        Animators = new Vector<Animator>();

        BoundList = new Vector<Bounds>();
        Contacts = new Vector<Contact>();
        Targets = new Vector<Vector2>();

        Slots.Resize(8);
        Slots.Extend(7);

        SetupActionTable();
        SetupCollisionTable();
    }

    public void SetupActionTable()
    {
        ActionTable[(int)Data.Action.None] = (Game g, int i, ref Animator a) => {
            // no-op
        };
        // action used for the null animation, objects that reach null
        // animation need to be deleted.
        ActionTable[(int)Data.Action.Destroy] = (Game g, int i, ref Animator a) => {
            //Debug.Log("Destroy!");
            g.DestroyEntity(g.EntityFromIndex(i));
        };
    }

    public static ushort MakeKey(ObjType ao, ObjType bo)
    {
        ushort r;
        byte a = (byte)ao;
        byte b = (byte)bo;
        if (a > b)
        {
            r = (ushort)((a << 8) | b);
        }
        else
        {
            r = (ushort)((a << 8) | b);
        }
        return r;
    }

    public void SetupCollisionTable()
    {
        // a collision between enemy and bullet
        CollisionTable[MakeKey(ObjType.Enemy, ObjType.Bullet)] = (Game g, ref Contact c) =>
        {
            if (c.Touch() == true)
            {
                if (g.Animators[c.B.index].node != data.GetNode(Data.Ani.Null))
                {
                    g.Animators[c.A.index].count = 0;
                    g.Animators[c.A.index].node = data.GetNode( (Data.Ani)((int)Data.Ani.BoomPow + Rand.Next(3)) );
                    g.Bodies[c.A.index].velocity = new Vector2(Const.v0, Const.v0);
                    g.Bodies[c.A.index].angle = -Scaler.Pi / Const.v2;
                    g.Components[c.A.index] &= (ushort)(~Cp.Enemy);
                    g.Animators[c.B.index].node = data.GetNode(Data.Ani.Null);
                    //++g.badieCount
                }
            }
        };
    }

    public void Init(int max)
    {
        BadieCount = 100;
        MaxActivate = 200;
        MaxContact = 1024;
        MaxEntity = max;

        EntityManager.Resize(MaxEntity);
        Generation.Resize(MaxEntity);
        ObjectType.Resize(MaxEntity);
        Bodies.Resize(MaxEntity);
        Components.Resize(MaxEntity);
        Players.Resize(MaxEntity);
        Enemies.Resize(MaxEntity);
        Animators.Resize(MaxEntity);

        BoundList.Resize(MaxEntity);

        Contacts.Resize(MaxContact);
        Activated.Resize(MaxActivate);
        Targets.Resize(8);

        Entity entity;

        for (int j = 0; j < Slots.Size; ++j)
        {
            if (Slots[j].Connected == true)
            {
                entity = CreateEntity();
                if (entity != Entity.Null)
                {
                    int i = entity.index;
                    ref var c = ref Components.Insert(i).Ref;
                    ref var b = ref Bodies.Insert(i).Ref;
                    ref var a = ref Animators.Insert(i).Ref;
                    ref var o = ref ObjectType.Insert(i).Ref;
                    ref var p = ref Players.Insert(i).Ref;

                    p.slot = (sbyte)j;
                    c = Mask(Cp.Body | Cp.Type | Cp.Player | Cp.Animator);

                    a.count = 0;
                    a.node = data.GetNode(Data.Ani.ManIdle);

                    b.position = new Vector2(Const.v0, Const.v0);
                    b.velocity = new Vector2(Const.v0, Const.v0);
                    b.displacement = new Vector2(Const.v0, Const.v0);
                    b.radius = Const.v0_15;
                    b.damping = Const.v0_003;
                    b.invMass = Const.v1over100;

                    Activate(entity);
                }
                //player = entity;
            }
        }

    }

    public ushort Mask(Cp flags)
    {
        return (ushort)(flags);
    }

    public bool TestMask(int i, Cp flags)
    {
        return (Components[i] & (ushort)(flags)) == (ushort)(flags);
    }

    public bool TestMask(int i, ushort flags)
    {
        return (Components[i] & flags) == flags;
    }

    public void Activate(Entity entity)
    {
        Activated[Activated.Append()] = entity.index;
    }

    public bool Valid(Entity entity)
    {
        return entity != Entity.Null && entity.generation == Generation[entity.index];
    }

    public Entity EntityFromIndex(int index)
    {
        return new Entity(Generation[index], index);
    }

    public Entity CreateEntity()
    {
        Entity entity = Entity.Null;
        int value = EntityManager.Remove();
        if (value != ValueSet.EndOfList)
        {
            //Debug.Log($"value: {value}");

            Generation.Extend(value);
            ObjectType.Extend(value);
            Bodies.Extend(value);
            Components.Extend(value);
            Players.Extend(value);
            Enemies.Extend(value);
            Animators.Extend(value);

            entity.index = value;
            entity.generation = Generation[entity.index];
        }
        OnCreate(entity);
        return entity;
    }

    public void DestroyEntity(Entity entity)
    {
        OnDestroy(entity);
        ++Generation[entity.index];
        EntityManager.Restore(entity.index);
        Components[entity.index] = 0x0000;
    }

    public void Update()
    {
        ResolveContacts();
        Integrate();
        ////ComputeContacts();

        ////NewPlayers();
        NewBadies();

        RunAnimators();
        UpdatePlayers();
        UpdateEnemies();

        CollisionMessages();

        ActivateEntities();

        SetDisplacements();

        FillContactList();
    }

    public void NewBadies()
    {
        var countBadies = 0;
        var mask = Mask(Cp.Active | Cp.Enemy);
        for (int i = 0; i < MaxEntity; ++i)
        {
            if (TestMask(i, mask))
            {
                ++countBadies;
            }
        }

        countBadies = BadieCount - countBadies;

        Entity entity;

        for (int j = 0; j < countBadies; ++j)
        {
            entity = CreateEntity();
            if (entity != Entity.Null)
            {
                int i = entity.index;
                ref var c = ref Components.Insert(i).Ref;
                ref var b = ref Bodies.Insert(i).Ref;
                ref var a = ref Animators.Insert(i).Ref;
                ref var o = ref ObjectType.Insert(i).Ref;
                ref var e = ref Enemies.Insert(i).Ref;

                e.target = Entity.Null;

                c = Mask(Cp.Body | Cp.Type | Cp.Enemy | Cp.Animator);

                o = (byte)ObjType.Enemy;

                a.count = 0;
                a.node = data.GetNode(Data.Ani.GreenRun);
                
                b.position = new Vector2(Const.v4, Scaler.Zero);

                //Scaler cos = Scaler.Cos(new Scaler(j) * Const.v0_3);
                //Scaler sin = Scaler.Sin(new Scaler(j) * Const.v0_3);
                //b.position = new Vector2(b.position.x * cos - b.position.y * sin, b.position.x * sin + b.position.y * cos);
                Scaler angle = (Rand.Next(359) * 2 * Scaler.Pi) / 360;
                Vector2 vec = new Vector2(Const.v1, Const.v0);
                vec = vec.Rotate(angle) * 16;
                b.position = vec;

                b.velocity = new Vector2(Const.v0, Const.v0);
                b.displacement = new Vector2(Const.v0, Const.v0);
                b.radius = Const.v0_15;
                b.damping = Const.v0_003;
                b.invMass = Const.v1over100;

                Activate(entity);
            }
        }
    }

    public void RunAnimators()
    {
        var mask = Mask(Cp.Active | Cp.Animator);
        for (int i = 0; i < MaxEntity; ++i)
        {
            if (TestMask(i, mask))
            {
                ref var a = ref Animators[i];
                ++a.count;
                if (a.count > data.FrameTable[a.node].durration)
                {
                    a.count = 0;
                    a.node = (ushort)(data.FrameTable[a.node].next);
                }
                // run action
                // game, entity index, animator data
                //data.RunAction()
                ActionTable[(ushort)(data.FrameTable[a.node].action)](this, i, ref a);
            }
        }
    }

    public void ResolveContacts()
    {
        Scaler relNv, remove, imp, newImpulse, change, bias;
        for (int j = 0; j < 10; ++j)
        {

            for (int i = 0; i < Contacts.Size; ++i)
            {

                ref Contact c = ref Contacts[i];

                if (Valid(c.A) && Valid(c.B))
                {
                    ref Body a = ref Bodies[c.A.index];
                    ref Body b = ref Bodies[c.B.index];

                    if (c.overlap < Scaler.Zero)
                    {
                        bias = Const.v0_2;
                    }
                    else
                    {
                        bias = Const.v1;
                    }

                    relNv = (a.displacement - b.displacement).Dot(ref c.normal);
                    //relNv = Vector2.Dot(a.displacement - b.displacement, c.normal);

                    remove = relNv + (bias * c.overlap);
                    imp = remove / (c.invMassA + c.invMassB);
                    newImpulse = Scaler.Min(imp + c.impulse, Const.v0);
                    change = newImpulse - c.impulse;

                    c.impulse = newImpulse;

                    //Debug.Log($"change: {change}");

                    a.displacement -= c.normal * c.invMassA * change;
                    b.displacement += c.normal * c.invMassB * change;
                }

            }


        }
    }

    public void FillContactList()
    {
        BoundList.Clear();
 
        var mask = Mask(Cp.Active | Cp.Body);
        for (int i = 0; i < MaxEntity; ++i)
        {
            if (TestMask(i, mask))
            {
                int a = BoundList.Size;
                if (a < MaxEntity)
                {
                    int bi = BoundList.Append();
                    ref Body b = ref Bodies[i];
                    BoundList[bi] = new Bounds(EntityFromIndex(i), b.position, b.velocity, b.radius);
                }
            }
        }

        BoundList.Sort((ref Bounds lhs, ref Bounds rhs) => {
            return lhs.lower.x < rhs.lower.x;
        });

        Contacts.Clear();

        for (int i = 0; i < BoundList.Size; ++i)
        {
            ref Bounds iter = ref BoundList[i];
            for (int j = i + 1; j < BoundList.Size; ++j)
            {
                ref Bounds nextIter = ref BoundList[j];
                if (iter.Overlap(ref nextIter) == true)
                {
                    if (Contacts.Size < MaxContact)
                    {
                     
                        ref Body a = ref Bodies[iter.entity.index];
                        ref Body b = ref Bodies[nextIter.entity.index];

                        var norm = a.position - b.position;
                        var len = norm.Length();

                        //Debug.Log($"norm: {(float)len},{(float)Scaler.Epsilon}");

                        if (len < Scaler.Epsilon)
                        {
                            norm = new Vector2(Const.v1, Const.v0);
                        }
                        else
                        {
                            norm.Normalize();
                        }

                        var ci = Contacts.Append();

                        ref Contact c = ref Contacts[ci];
                        c.A = iter.entity;
                        c.B = nextIter.entity;
                        c.invMassA = a.invMass;
                        c.invMassB = b.invMass;
                        //Debug.Log($"norm: {norm.x},{norm.y}");
                        c.normal = norm;
                        c.overlap = len - (a.radius + b.radius) + Contact.SMALL_SLOP;
                        c.impulse = Const.v0;

                    }
                }
            }
        }

        

    }

    public void SetDisplacements()
    {
        Scaler mag;
        var mask = Mask(Cp.Active | Cp.Body);
        for (int i = 0; i < MaxEntity; ++i)
        {
            if (TestMask(i, mask))
            {
                ref var b = ref Bodies[i];
                b.displacement = b.velocity;
                mag = b.velocity.Length();
                if (mag <= b.damping)
                {
                    b.velocity = new Vector2(Const.v0, Const.v0);
                }
                else
                {
                    b.velocity -= b.velocity.NormalizedCopy() * b.damping;
                }

            }
        }
    }

    public void Integrate()
    {
        var mask = Mask(Cp.Active | Cp.Body);
        for (int i = 0; i < MaxEntity; ++i)
        {
            if (TestMask(i, mask))
            {
                ref var b = ref Bodies[i];
                b.position += b.displacement;
            }
        }
    }

    public void UpdateEnemies()
    {
        var mask = Mask(Cp.Active | Cp.Body | Cp.Enemy);
        for (int i = 0; i < MaxEntity; ++i)
        {
            if (TestMask(i, mask))
            {
                ref var e = ref Enemies[i];
                ref var b = ref Bodies[i];

                b.velocity = new Vector2(Const.v0, Const.v0);

                Scaler smallest = 0;
                var target = -1;
                for (int j = 0; j < Targets.Size; ++j)
                {
                    ref Vector2 p = ref Targets[j];
                    var len = (p - b.position).LengthSquared();
                    if (target == -1 || len < smallest)
                    {
                        target = j;
                        smallest = len;
                    }
                }

                if (target != -1)
                {
                    ref Vector2 p = ref Targets[target];
                    b.velocity = (p - b.position).NormalizedCopy();
                    b.velocity = b.velocity * Const.v0_062 * Const.v0_25;
                }

                b.angle = b.velocity.Angle();
            }
        }
    }

    public void UpdatePlayers()
    {
        Targets.Clear();

        int pcount = 0;

        Vector2 move;
        var mask = Mask(Cp.Active | Cp.Body | Cp.Player);
        for (int i = 0; i < MaxEntity; ++i)
        {
            if (TestMask(i, mask))
            {
                pcount++;

                ref var p = ref Players[i];
                ref var b = ref Bodies[i];
                ref var a = ref Animators[i];

                ref var slot = ref Slots[p.slot];
                move = new Vector2(Scaler.Zero, Scaler.Zero);

                if (slot.Input.Left)     move.x = Const.vn1;
                if (slot.Input.Right)    move.x = Const.v1;
                if (slot.Input.Up)       move.y = Const.v1;
                if (slot.Input.Down)     move.y = Const.vn1;

                move.Normalize();

                var target = new Vector2(slot.Input.X, slot.Input.Y);
                target.Normalize();

                b.angle = target.Angle();

                //b.velocity = Const.v0_02333333333333333 * move;
                b.velocity = Const.v0_02333333333333333 * Const.v2 * move;

                var tar = Targets.Append();
                Targets[tar] = b.position;


                if (p.delayFire > 0) { p.delayFire--; }
                

                if (slot.Input.Primary && p.delayFire == 0)
                {
                    p.delayFire = 5;

                    a.count = 0;
                    a.node = data.GetNode(Data.Ani.ManShoot);

                    Entity entity = CreateEntity();
                    if (entity != Entity.Null)
                    {
                        int index = entity.index;
                        ref var cn = ref Components.Insert(index).Ref;
                        ref var bn = ref Bodies.Insert(index).Ref;
                        ref var an = ref Animators.Insert(index).Ref;
                        ref var on = ref ObjectType.Insert(index).Ref;

                        cn = Mask(Cp.Body | Cp.Type | Cp.Animator);

                        on = (byte)ObjType.Bullet;

                        an.count = 0;
                        an.node = data.GetNode(Data.Ani.Bullet);

                        bn.position = b.position + target * Const.v0_22;
                        bn.velocity = target * Const.v0_1;
                        bn.displacement = new Vector2(Const.v0, Const.v0);
                        bn.angle = bn.velocity.Angle();
                        bn.radius = Const.v0_04;
                        bn.damping = Const.v0;
                        bn.invMass = Const.v1;

                        Activate(entity);
                    }

                }

            }
        }

        

    }

    public void CollisionMessages()
    {
        for (int i = 0; i < Contacts.Size; ++i)
        {
            ref var c = ref Contacts[i];
            if (Valid(c.A) && Valid(c.B))
            {
                byte a = ObjectType[c.A.index];
                byte b = ObjectType[c.B.index];
                ushort key = MakeKey((ObjType)a, (ObjType)b);
                if (CollisionTable.ContainsKey(key))
                {
                    if (a < b)
                    {
                        var ic = c.Invert();
                        CollisionTable[key](this, ref ic);
                    }
                    else
                    {
                        CollisionTable[key](this, ref c);
                    }
                }
            }
        }
    }
    
    public void ActivateEntities()
    {
        for (int i = 0; i < Activated.Size; ++i)
        {
            Components[Activated[i]] |= (ushort)Cp.Active;
        }
        Activated.Clear();
    }
    

}
